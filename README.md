<div align="center">
<a href="https://www.giuseppetavera.it"><img src="./img/giusetavera-foto.jpg" alt="Giuseppe Tavera - Web Developer, Copywriter and Digital Creator" width="200"></a>
<h2>Giuseppe Tavera</h2>

<h3>Web Developer, Copywriter<br>and Digital Creator</h3>
</div>

<div align="center">
  <a target="_blank" title="Giuseppe Tavera - WordPress Developer Treviso" href="https://www.instagram.com/giusetavera/">
    <img src="./img/social/icona-instagram.png" width="40"
         alt="Giuseppe Tavera - Instagram">
  </a>
  <a target="_blank" title="Giuseppe Tavera - WordPress Developer Treviso" href="https://www.linkedin.com/in/giusetavera/">
    <img src="./img/social/icona-linkedin.png" width="40" 
         alt="Giuseppe Tavera - Linkedin">
  </a>
  <a target="_blank" title="Giuseppe Tavera - WordPress Developer Treviso" href="https://www.giuseppetavera.it">
    <img src="./img/social/icona-web.png" width="40" 
         alt="Giuseppe Tavera - sito web">
  </a>
  <a target="_blank" title="Giuseppe Tavera - WordPress Developer Treviso" href="mailto:info@giuseppetavera.it">
    <img src="./img/social/icona-mail.png" width="40" 
         alt="Giuseppe Tavera - contatti mail">
  </a>
</div>
<br>

<div align="center">
I am not a traditional web developer or SEO or copywriter either. I am the guy that is <strong>pretty keen on... doing things</strong> :)<br>Let's say I like creating my own project and that's fine.
</div>
<br>
<div align="center">
  <a href="#frontend">Frontend</a> •
  <a href="#backend">Backend</a> •
  <a href="#devops">DevOps</a> •
  <a href="#other-tools">Other tools</a>
</div>
<br>

<div align="center">
<img src="./img/giusetavera-foto-orizzontale.png" alt="Giuseppe Tavera - Web Developer, Copywriter and Digital Creator">
</div>

## Frontend

As Frontend Developer I worked on custom **WordPress templates**, **mini-sites** and **landing pages** as well. Recently I give a chance to new modern Javascript frameworks to give life to always more complex UIs.

![Markdown](https://img.shields.io/badge/-%20Markdown%20-white "Markdown")
![SCSS](https://img.shields.io/badge/-%20SCSS%20-red "SCSS")
![Bootstrap 5](https://img.shields.io/badge/-%20Bootstrap%205-purple "Bootstrap 5")
![Tailwind CSS](https://img.shields.io/badge/-%20Tailwind%20-cyan "Tailwind CSS")
![React](https://img.shields.io/badge/-%20React%20-blue "React")
![Vue](https://img.shields.io/badge/-%20Vue%20-brightgreen "Vue")
![Angular](https://img.shields.io/badge/-%20Angular%20-red "Angular")
![Gulp](https://img.shields.io/badge/-%20Gulp%20-orange "Gulp")
![Electron](https://img.shields.io/badge/-%20Electron%20-cyan "Electron")

Long story short:

- HTML 5, Markdown
- CSS (RWD and Flexbox)
- SCSS (variables, mixins)
- Bootstrap, Tailwind
- React (sometimes with Redux)
- Vue JS and Angular (just a try)
- Gulp
- Electron

## Backend

I spent most of my life on **WordPress**, all my projects are based on this amazin CMS I learnt to bend to my will. So it is all about PHP with a few other stuff.

![WordPress](https://img.shields.io/badge/-%20WordPress%20-blue "WordPress")
![PHP](https://img.shields.io/badge/-%20PHP%20-purple "PHP")
![MySQL](https://img.shields.io/badge/-%20MySQL%20-blue "MySQL")
![Laravel](https://img.shields.io/badge/-%20Laravel%20-red "Laravel")
![Python](https://img.shields.io/badge/-%20Python%20-yellow "Python")

- WordPress
- PHP, MySQL
- Laravel
- Python

## DevOps

I call this section _DevOps_ just to simply. I've been a **System Administrator** for a couple of years and did a good job even if I had never opened a CLI before in my life. I used to configure local, staging and production environment in LAMP stack for CMS like WordPress, Moodle and Magento.

![AWS](https://img.shields.io/badge/-%20AWS%20-orange "AWS")
![Vagrant](https://img.shields.io/badge/-%20Vagrant%20-blue "Vagrant")
![GitLab](https://img.shields.io/badge/-%20GitLab%20-orange "GitLab")
![Bash](https://img.shields.io/badge/-%20Bash%20-white "Bash")

I learnt lots of new skills, such as:

- Apache web server
- Amazon Web Services (I used it in a couple of situations)
- providers like Digital Ocean, Hetzner and Siteground
- Vagrant (I created my custom box for WordPress)
- Gitlab, GitHub and Git commands
- Bash (enough to create a simple deploy script)

## Other tools

I love creating my own projects. They requires lots of different skills and some times also different teams, in order to accomplish the final result. For this reason **I am kind of a project manager myself**.

This section sum up all other topic, tools and software that became part of my workflow routine.

![Notion](https://img.shields.io/badge/-%20Notion%20-white "Notion")
![Photoshop](https://img.shields.io/badge/-%20Photoshop%20-blue "Photoshop")
![ScreenFlow](https://img.shields.io/badge/-%20ScreenFlow%20-white "ScreenFlow")
![Visual Studio Code](https://img.shields.io/badge/-%20Visual%20Studio%20Code%20-white "Visual Studio Code")
![Termius](https://img.shields.io/badge/-%20Termius%20-black "Termius")
![Google Analytics](https://img.shields.io/badge/-%20Google%20Anlytics%20-orange "Google Analytics")
![Google Drive](https://img.shields.io/badge/-%20Google%20Drive%20-brightgreen "Google Drive")

- Notion (workspace), Jira (task manager)
- Photoshop, Figma and Adobe XD for prototyping
- Screenflow and DaVinci Resolve (video editing)
- Visual Studio Code (code editor)
- Termius (SSH client)
- Google Analytics, Google Search Console
- KeywordsPro, Answer The Public
- Google Drive

<br><br><br>

---

&copy; 2022 | Giuseppe Tavera
